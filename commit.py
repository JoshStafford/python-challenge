import os
from time import sleep
from sys import argv, exit
from threading import Thread

libs = ["tqdm", "keyboard", "win10toast"]

try:
	from tqdm import tqdm as bar
	import keyboard
	from win10toast import ToastNotifier
	print("Libraries successfully imported.")

except:

	decision = input("Necessary libraries not installed. Would you like to install them? [y / n]\n> ")

	while(decision != "y" and decision != "n"):
		print("Invalid input.")
		decision = input("Necessary libraries not installed. Would you like to install them? [y / n]\n> ")

	if(decision == "y"):

		print("Attempting to install and import libraries.")
		try:
			for lib in libs:
				os.system("pip3 install " + lib)

			from tqdm import tqdm as bar
			import keyboard
			from win10toast import ToastNotifier

		except:
			for lib in libs:
				os.system("pip install " + lib)

			from tqdm import tqdm as bar
			import keyboard
			from win10toast import ToastNotifier

	else:

		print("Terminating program.")
		exit()


toaster = ToastNotifier()
commands = ["git add *", "git commit -m Auto", "git push -u"]
pause = float(argv[1]) * 60
count = pause

stopping = False

sleeping = True
choosing = False
default_msg = "Auto"
msg = ""

def run_commit(arg):
	global count
	global stopping
	global sleeping
	global default_msg
	global choosing

	print("""Directory committed every %f minutes\n
		Press \'-\' AND \'+\' to customise message and commit manually\n
		Press \'0\' AND \'+\' to exit the program\n
		## Do NOT CTRL-C ##""" % (float(argv[1])))

	while stopping == False:

		print("\n===> Sleeping <===\n")
		for i in bar(range(int(pause))):
			if stopping == True:
				break

			if count == 30:
				print("Notifying")
				toaster.show_toast("Notification!", "30 seconds to commit.", threaded=True,
                   icon_path=None, duration=3)  # 3 seconds

			if sleeping:
				count -= 1
				sleep(1)
			else:
				sleeping = True
				break

		# sleep(pause)
		if stopping == False:
			if choosing == True:
				msg = input("Enter commit message\n> ")
				set_msg(msg)

			print("\n===> Committing <===\n")
			for c in commands:
				# print(c)
				os.system(c)
				count = pause
				choosing = False
				set_msg(default_msg)
			# os.system("git add *")
			# os.system("git commit -m 'Auto'")
			# os.system("git push -u")

#Test

def set_msg(msg):
	global commands
	commands[1] = "git commit -m " + msg



def check_press(arg):
	global sleeping
	global stopping
	global choosing
	while True:
		try:
			if keyboard.is_pressed('-') and keyboard.is_pressed('='):
				sleeping = False
				choosing = True
			else:
				pass
		except:
			break

		try:
			if keyboard.is_pressed('0') and keyboard.is_pressed('='):
				print("\n===> Exiting <===\n")
				stopping = True
				exit()
			else:
				pass
		except:
			break


if __name__ == "__main__":
	thread1 = Thread(target = run_commit, args = (0, ))
	thread2 = Thread(target = check_press, args = (0, ))
	print("Creating threads")
	thread1.start()
	thread2.start()
	thread1.join()
	thread2.join()
