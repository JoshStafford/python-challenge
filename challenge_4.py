# http://www.pythonchallenge.com/pc/def/linkedlist.php

import urllib.request
from bs4 import BeautifulSoup

url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="
num = 8022

def get_next(num):

    global url
    page = urllib.request.urlopen(url + str(num)).read()

    soup = BeautifulSoup(page).text
    print(soup)
    contents = soup.split(' ')
    for word in contents:

        try:
            number = int(word)
            print(number)
            get_next(number)
        except:
            pass
