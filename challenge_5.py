# http://www.pythonchallenge.com/pc/def/peak.html
# http://www.pythonchallenge.com/pc/def/pickle.html

import pickle

with open("challenge_5_text_unix.pkl", "rb") as f:

    new_dict = pickle.load(f)

    contents = []

    for i in range(len(new_dict)):

            for j in range(len(new_dict[i])):

                contents.append(new_dict[i][j])

# Contents appears to be runlength compressed image using hashes and spaces

# Factors of 2185 for possible number of rows
# 1, 5, 19, 23, 95, 115, 437, 2185

result = ""

for entry in contents:

    result += entry[0] * entry[1]

line = ""
factor = 23
row = int(len(result) / factor)
print(row)
for i in range(factor):

    for j in range(row):

        line += result[i*row + j]

    print(line)
    line = ""
