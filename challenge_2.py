with open("challenge_2_text.txt", "r") as f:

    text = f.read()
    special_chars = {}

    for char in text:

        if(char in special_chars):
            special_chars[char] += 1
        else:
            special_chars[char] = 1

    print(special_chars)
