# http://www.pythonchallenge.com/pc/def/channel.html

start = 90052

def get_next(num):
    next_num = None
    with open("channel/" + str(num) + ".txt", "r") as f:

        # print(f.read())
        words = f.read()
        words = words.split(" ")
        # print(words)

        for phrase in words:
            try:
                next_num = int(phrase)
                print(next_num)
                break
            except:
                continue

    print("Next number is %d" % (next_num))
    get_next(next_num)

# Last file is 46145
# 'Collect the commments.'
