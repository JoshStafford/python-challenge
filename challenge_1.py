# http://www.pythonchallenge.com/pc/def/map.html

def cypher(inp, key=2):

    result = ""
    alpha = "abcdefghijklmnopqrstuvwxyz"

    for letter in inp:

        if(letter in alpha):
            index = alpha.index(letter)
            index = (index + key) % 26
            result += alpha[index]

        else:
            result += letter

    return result
